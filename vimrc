syntax enable "resalta syntax
set showcmd "muestra comandos
set ruler "donde estamos
set cursorline
set encoding=utf-8
set showmatch 
set sw=2 "indentacion
set relativenumber

set number

set mouse=a "interactua con el raton
set numberwidth=1 "setea ancho
set clipboard=unnamed 
set laststatus=2 "siempre es visible el status
set noshowmode "no muestra el modo


call plug#begin('~/.vim/plugged')

" Temas
Plug 'morhetz/gruvbox'
 
 " IDE
Plug 'easymotion/vim-easymotion' " Salta por partes del codigo
Plug 'scrooloose/nerdtree'
Plug 'christoomey/vim-tmux-navigator' " con Control h j k l nos vamos moviendo por las ventanas.

call plug#end()



colorscheme gruvbox 
let g:gruvbox_contrast_dark = "hard"
let NERDTreeQuitOnOpen=1 " Cuando se abre el archivo cierra NerdTree

" La tecla leader es el spacio
let mapleader=" "


" Atajo easymotion
" nmap es atajo que solo funciona en modo normal.
nmap <Leader>s <Plug>(easymotion-s2)
nmap <Leader>ft :NERDTreeFind<CR> " <CR> significa enter

nmap <Leader>w :w<CR>
nmap <Leader>q :q<CR>

