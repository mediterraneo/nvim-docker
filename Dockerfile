FROM alpine:latest

# Instalamos las dependencias
RUN apk update && \
    apk add vim && \
    apk add neovim && \
    apk add git && \
    apk add curl

# RUN  curl -fLo ~/.local/share/nvim/site/autoload/plug.vim --create-dirs https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim



RUN addgroup -S appgroup && adduser -S appuser -G appgroup
USER appuser

WORKDIR /home/appuser

RUN mkdir -p .config/nvim

RUN sh -c 'curl -fLo "${XDG_DATA_HOME:-$HOME/.local/share}"/nvim/site/autoload/plug.vim --create-dirs \
       https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'


COPY init.vim .config/nvim/init.vim

COPY vimrc .config/.vimrc


RUN ln -s .config/.vimrc .vimrc
RUN ln -s .vim .config/

# CMD ["/bin/sh"]
CMD ["nvim"]
